const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  mode: 'development',
  target: 'node',
  watchOptions: {
    ignored: ['dist/*', 'node_modules/**'],
    poll: 1000,
  },
  entry: {
    handlers: './src/handlers/index.js',
  },
  devtool: 'inline-source-map',
  plugins: [
    new CleanWebpackPlugin(),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
    ],
  },
  output: {
    // filename: '[name].bundle.js',
    // path: path.resolve(__dirname, 'dist'),
    libraryTarget: 'commonjs',
    library: 'handlers',
  },
};
