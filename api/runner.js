require('dotenv').config({ path: `.env/${process.env.NODE_ENV || 'dev'}` });

const {
    handlers,
} = require('./dist/handlers');

const updateBody = require('./requests/update');
const readBody = require('./requests/read.json');
const deleteBody = require('./requests/delete.json');

(async () => {
    // console.log(await update.updateTask(updateBody));
    console.log(await handlers.default.delete(deleteBody));
})();   