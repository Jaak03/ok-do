/**
 * Generate single read parameters for DynamoDB.
 * @param {string} taskId
 * @param {string} authorId
 */
const singleReadParams = (taskId, authorId) => ({
  TableName: process.env.DYNAMODB_TABLE,
  KeyConditionExpression: '#authorId = :authorId and #taskId = :taskId',
  ExpressionAttributeNames: {
    '#authorId': 'authorId',
    '#taskId': 'taskId',
  },
  ExpressionAttributeValues: {
    ':taskId': taskId,
    ':authorId': authorId,
  },
});

/**
 * Get the params for reading a list of tasks per user from DynamoDB.
 * @param {string} authorId
 */
const listReadParams = (authorId) => ({
  TableName: process.env.DYNAMODB_TABLE,
  KeyConditionExpression: '#authorId = :authorId',
  ExpressionAttributeNames: {
    '#authorId': 'authorId',
  },
  ExpressionAttributeValues: {
    ':authorId': authorId,
  },
});

/**
 * Get the parameters to update a tasks' state and note.
 * @param {object} update
 */
const updateTaskParams = ({
  authorId, taskId, state, note,
}) => {
  const ExpressionAttributeValues = {};
  let UpdateExpression = 'SET';

  /*
    If the first if is triggered and the state is set, the comma flag causes
    the following if statement to add a comma before the next addition to the
    UpdateExpression.
  */
  let commaFlag = false;
  if (state) {
    ExpressionAttributeValues[':state'] = state;
    UpdateExpression = `${UpdateExpression} state = :state`;

    commaFlag = true;
  }

  if (note) {
    ExpressionAttributeValues[':note'] = note;
    UpdateExpression = `${UpdateExpression}${commaFlag ? ',' : ''} note = :note`;
  }

  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Key: {
      authorId,
      taskId,
    },
    UpdateExpression,
    ExpressionAttributeValues,
    ReturnValues: 'ALL_NEW',
  };

  return params;
};

/**
 * Get the params for deleting a specific task from dynamoDB.
 * @param {object} taskDetails { authorId, taskId }
 */
const deleteTaskParams = ({
  authorId, taskId,
}) => ({
  TableName: process.env.DYNAMODB_TABLE,
  Key: {
    authorId,
    taskId,
  },
});

/**
 * Reads an item from dynamodb if one exists.
 * @param {object} logger winston instance
 * @param {object} parameters
 * @param {object} dynamoClient
 */
async function readSingleTask(logger, { taskId, authorId }, client) {
  logger({ message: 'Reading Item from DynamoDB' });

  const params = singleReadParams(taskId, authorId);

  logger({
    message: JSON.stringify({
      arguments: {
        taskId,
        authorId,
      },
      params,
    }),
  });

  const result = await client.query(params).promise();

  logger({ level: 'debug', message: result });

  if (!result.Items) {
    throw new Error('Items not found.');
  }

  return result;
}

/**
 * Retrieves a list of tasks from DynamoDB for the specified author.
 * @param {object} logger winston instance.
 * @param {object} {authorId} author to get tasks for.
 * @param {object} client
 */
async function readListOfTasksForAuthor(logger, { authorId }, client) {
  logger({ message: 'Reading Item from DynamoDB' });

  const params = listReadParams(authorId);

  logger({
    message: JSON.stringify({
      arguments: {
        authorId,
      },
      params,
    }),
  });

  const result = await client.query(params).promise();

  logger({ level: 'debug', message: result });

  if (!result.Items) {
    throw new Error('Items not found.');
  }

  return result;
}

/**
 * Updates the values of the note and state fields for a specific task.
 * @param {object} logger winston instance.
 * @param {object} task details for the task update.
 * @param {object} client
 */
async function updateTask(logger, update, client) {
  logger({ message: 'Updating the task fields.' });
  logger({ message: update, level: 'debug' });
  const params = updateTaskParams(update);

  logger({
    message: {
      arguments: {
        update,
      },
      params,
    },
  });

  const result = await client.update(params).promise();

  logger({ level: 'debug', message: result });

  if (!result.Attributes) {
    throw new Error('Item not found.');
  }

  return result;
}

async function deleteTask(logger, { authorId, taskId }, client) {
  logger({ message: 'Deleting a task.' });

  const params = deleteTaskParams({ authorId, taskId });

  logger({
    message: {
      arguments: {
        authorId,
        taskId,
      },
      params,
    },
  });

  const result = await client.delete(params).promise();

  logger({ level: 'debug', message: result });

  return { authorId, taskId };
}

export {
  readSingleTask,
  readListOfTasksForAuthor,
  updateTask,
  deleteTask,
};
