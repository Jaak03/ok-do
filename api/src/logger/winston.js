/* eslint-disable no-unused-vars */
import { createLogger, format, transports as t } from 'winston';
import winston from 'winston/lib/winston/transports';

const {
  combine,
  timestamp,
  label,
  prettyPrint,
  errors,
} = format;

// https://github.com/winstonjs/winston#creating-your-own-logger
const loggingLevels = {
  error: 0,
  warn: 1,
  info: 2,
  debug: 3,
};

/**
 * Default winston logger to be expanded in the future.
 *
 * @param service label to show where the log is coming from.
 * @returns winston logger.
 */
export default (
  service = 'general',
) => ({
  level = 'info',
  message = '',
}) => createLogger({
  level: process.env.LOGGING_LEVEL,
  levels: loggingLevels,
  format: combine(
    errors({ stack: true }),
    label({ label: service }),
    timestamp(),
    prettyPrint(),
  ),
  transports: [
    new t.Console(),
  ],
}).log(level, message);
