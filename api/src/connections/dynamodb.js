import AWS from 'aws-sdk';

let clientConnection;

AWS.config.update({
  region: 'us-east-1',
  httpOptions: {
    timeout: 6000,
    connectTimeout: 500,
  },
  maxRetries: 5,
});

/**
 * Returns the cached connection as a preference.
 */
export default async () => {
  if (!clientConnection) {
    clientConnection = new AWS.DynamoDB.DocumentClient();
    return clientConnection;
  }

  return clientConnection;
};
