import * as uuid from 'uuid';

import dynamoClient from '../connections/dynamodb';

/**
 * Handler function to put a task on DynamoDB.
 *
 * @param {object} event { author}
 */
async function create(event) {
  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Item: {
      authorId: event.body.author,
      taskId: uuid.v1(),
      title: event.body.title,
      note: event.body.note,
      createdAt: Date.now(),
    },
  };

  try {
    const client = await dynamoClient();
    await client.put(params).promise();

    return {
      statusCode: 201,
      body: JSON.stringify(params.Item),
    };
  } catch (e) {
    return {
      statusCode: 500,
      body: JSON.stringify({ error: e.message }),
    };
  }
}

export default create;
