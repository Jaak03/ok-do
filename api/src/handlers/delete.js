/* eslint-disable no-unused-vars */
import getClient from '../connections/dynamodb';
import logger from '../logger/winston';

import { deleteTask } from '../libs/dynamodb';

/**
 * Handler function to delete a task on DynamoDB.
 *
 * @param {object} event { pathParameters }
 */
async function deleteTaskForAuthorIdAndTaskId(event) {
  const log = logger('deleteTask');

  try {
    const client = await getClient();
    const result = await deleteTask(
      log,
      {
        taskId: event.pathParameters.taskId,
        authorId: 'jaak',
      },
      client,
    );

    return {
      statusCode: 200,
      body: JSON.stringify(result),
    };
  } catch (e) {
    log({
      level: 'error',
      message: e,
    });
    return {
      statusCode: 500,
      body: e.message,
    };
  }
}

export default deleteTaskForAuthorIdAndTaskId;
