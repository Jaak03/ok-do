import getClient from '../connections/dynamodb';
import logger from '../logger/winston';

import {
  readSingleTask,
  readListOfTasksForAuthor,
} from '../libs/dynamodb';

/**
 * Handler function to read a task from dynamoDB by id.
 *
 * @param {object} event { author}
 */
async function getTask(event) {
  const log = logger('getTask');
  try {
    const client = await getClient();
    const result = await readSingleTask(
      log,
      {
        taskId: event.pathParameters.taskId,
        authorId: 'jaak',
      },
      client,
    );

    if (result.Items.length > 1) {
      return {
        statusCode: 200,
        body: JSON.stringify(result.Items),
      };
    }

    return {
      statusCode: 200,
      body: JSON.stringify(result.Items[0]),
    };
  } catch (e) {
    log({
      level: 'error',
      message: e,
    });
    return {
      statusCode: 500,
      body: e.message,
    };
  }
}

async function listTasksPerAuthor(event) {
  const log = logger('getTask');

  log({ level: 'info', message: event });

  try {
    const client = await getClient();
    const result = await readListOfTasksForAuthor(
      log,
      {
        authorId: 'jaak',
      },
      client,
    );

    return {
      statusCode: 200,
      body: JSON.stringify(result.Items),
    };
  } catch (e) {
    log({
      level: 'error',
      message: e,
    });
    return {
      statusCode: 500,
      body: e.message,
    };
  }
}

export {
  getTask,
  listTasksPerAuthor,
};
