import create from './create';
import { getTask, listTasksPerAuthor } from './read';
import update from './update';
import deleteTask from './delete';

export default {
  create,
  read: { getTask, listTasksPerAuthor },
  update,
  delete: deleteTask,
};
