/* eslint-disable no-unused-vars */
import getClient from '../connections/dynamodb';
import logger from '../logger/winston';

import { updateTask } from '../libs/dynamodb';

/**
 * Handler function to update a task on DynamoDB.
 *
 * @param {object} event { pathParameters, body: { state, note } }
 */
async function updateStateOrNoteForTask(event) {
  const log = logger('updateTask');

  try {
    const client = await getClient();
    const result = await updateTask(
      log,
      {
        taskId: event.pathParameters.taskId,
        authorId: 'jaak',
        state: event.body.state,
        note: event.body.note,
      },
      client,
    );

    return {
      statusCode: 200,
      body: JSON.stringify(result.Attributes),
    };
  } catch (e) {
    log({
      level: 'error',
      message: e,
    });
    return {
      statusCode: 500,
      body: e.message,
    };
  }
}

export default updateStateOrNoteForTask;
